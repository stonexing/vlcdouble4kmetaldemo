//
//  VLCMedia.h
//  VLCDemo
//
//  Created by Asus on 2021/7/15.
//

#import <Foundation/Foundation.h>

#ifndef VLCMedia_h
#define VLCMedia_h

@interface VLCMedia : NSObject
+ (instancetype)mediaWithURL:(NSURL *)anURL;

/**
 * Manufactures a new VLCMedia object using the path specified.
 * \param aPath Path to the media to be accessed.
 * \return A new VLCMedia object, only if there were no errors.  This object will be automatically released.
 * \see initWithPath
 */
+ (instancetype)mediaWithPath:(NSString *)aPath;

@end


@interface VLCLibrary : NSObject

@property (readwrite, nonatomic) BOOL debugLogging;

@end

@interface VLCMediaPlayer : NSObject

@property (strong) id drawable; /* The videoView or videoLayer */

@property (NS_NONATOMIC_IOSONLY, strong) VLCMedia *media;

@property (NS_NONATOMIC_IOSONLY, getter=isPlaying, readonly) BOOL playing;

@property (nonatomic, readonly) VLCLibrary *libraryInstance;

@property (nonatomic) float scaleFactor;
@property (nonatomic) CGSize videoSize;



@property (nonatomic) float brightness;
@property (nonatomic) float saturation;
@property (nonatomic) float contrast;
@property (nonatomic) float hue;
@property (nonatomic) float gamma;



@property (NS_NONATOMIC_IOSONLY) char *videoAspectRatio;

//@property (NS_NONATOMIC_IOSONLY) char *videoCropGeometry;

- (void)play;

/**
 * Set the pause state of the feed. Do nothing if already paused.
 */
- (void)pause;

/**
 * Stop the playing.
 */
- (void)stop;


@end

typedef NS_ENUM(NSUInteger, VLCAspectRatio) {
    VLCAspectRatioDefault = 0,
    VLCAspectRatioFillToScreen,
    VLCAspectRatioFourToThree,
    VLCAspectRatioSixteenToNine,
    VLCAspectRatioSixteenToTen,
};

#endif /* VLCMedia_h */
