//
//  FFMpegVideoRenderShader.metal
//  MosaicMovieSwiftUI
//
//  Created by a on 2021/7/7.
//

#include <metal_stdlib>
using namespace metal;

struct SingleInputVertexIO
{
    float4 position [[position]];
    float2 textureCoordinate [[user(texturecoord)]];
};

vertex SingleInputVertexIO oneInputVertex(const device packed_float2 *position [[buffer(0)]],
                                          const device packed_float2 *texturecoord [[buffer(1)]],
                                          uint vid [[vertex_id]])
{
    SingleInputVertexIO outputVertices;
    
    outputVertices.position = float4(position[vid], 0, 1.0);
    outputVertices.textureCoordinate = texturecoord[vid];
    
    return outputVertices;
}


constant half3x3 colorConversionMatrix601Default = half3x3(
    1.164,  1.164, 1.164,
    0.0, -0.392, 2.017,
    1.596, -0.813,   0.0
                                                           );
//
// BT.601 full range (ref: http://www.equasys.de/colorconversion.html)
constant half3x3 colorConversionMatrix601FullRangeDefault = half3x3(
    1.0,    1.0,    1.0,
    0.0,    -0.343, 1.765,
    1.4,    -0.711, 0.0
                                                                    );
//
//// BT.709, which is the standard for HDTV.
constant half3x3 colorConversionMatrix709Default = half3x3(
    1.164,  1.164, 1.164,
    0.0, -0.213, 2.112,
    1.793, -0.533,   0.0
                                                           );



fragment half4 uyuvRenderFragment(SingleInputVertexIO fragmentInput [[stage_in]],
                                     texture2d<half> textureY [[texture(0)]],
                                     texture2d<half> textureU [[texture(1)]],
                                     texture2d<half> textureV [[texture(2)]])
{
    constexpr sampler quadSampler;
    half3 yuv;
    yuv.x = textureY.sample(quadSampler, fragmentInput.textureCoordinate).r;
    yuv.y = textureU.sample(quadSampler, fragmentInput.textureCoordinate).r - 0.5;
    yuv.z = textureV.sample(quadSampler, fragmentInput.textureCoordinate).r - 0.5;

    return half4(colorConversionMatrix709Default * yuv, 1);
}

fragment half4 p010RenderFragment(SingleInputVertexIO fragmentInput [[stage_in]],
                                     texture2d<half> textureY [[texture(0)]],
                                     texture2d<half> textureU [[texture(1)]],
                                     texture2d<half> textureV [[texture(2)]])
{
    constexpr sampler quadSampler;
    half3 yuv;
    yuv.x = textureY.sample(quadSampler, fragmentInput.textureCoordinate).r * 64.0;
    yuv.y = textureU.sample(quadSampler, fragmentInput.textureCoordinate).r * 64.0 - 0.5;
    yuv.z = textureV.sample(quadSampler, fragmentInput.textureCoordinate).r * 64.0 - 0.5;

    return half4(colorConversionMatrix709Default * yuv, 1);
}

fragment half4 y420RenderFragment(SingleInputVertexIO fragmentInput [[stage_in]],
                                     texture2d<half> textureY [[texture(0)]],
                                     texture2d<half> textureU [[texture(1)]],
                                     texture2d<half> textureV [[texture(2)]])
{
    constexpr sampler quadSampler;
    half3 yuv;
    yuv.x = textureY.sample(quadSampler, fragmentInput.textureCoordinate).r;
    yuv.y = textureU.sample(quadSampler, fragmentInput.textureCoordinate).r - 0.5;
    yuv.z = textureV.sample(quadSampler, fragmentInput.textureCoordinate).r - 0.5;

    return half4(colorConversionMatrix709Default * yuv, 1);
}


fragment half4 nv12RenderFragment(SingleInputVertexIO fragmentInput [[stage_in]],
                                     texture2d<half> textureY [[texture(0)]],
                                     texture2d<half> textureUV [[texture(1)]])
{
    constexpr sampler quadSampler;
    half3 yuv;
    yuv.x = textureY.sample(quadSampler, fragmentInput.textureCoordinate).r;
    yuv.yz = textureUV.sample(quadSampler, fragmentInput.textureCoordinate).rg - half2(0.5, 0.5);
    
    
    return half4(colorConversionMatrix709Default * yuv, 1.0);
    
}


fragment half4 rgbaRenderFragment(SingleInputVertexIO fragmentInput [[stage_in]],
                                     texture2d<half> textureRGBA [[texture(0)]])
{
    constexpr sampler quadSampler;
    return textureRGBA.sample(quadSampler, fragmentInput.textureCoordinate);
    
}
fragment half4 bgraRenderFragment(SingleInputVertexIO fragmentInput [[stage_in]],
                                     texture2d<half> textureRGBA [[texture(0)]])
{
    constexpr sampler quadSampler;
    return textureRGBA.sample(quadSampler, fragmentInput.textureCoordinate);
    
}


constant half3x3 matrix_xyz_rgb(3.240454 , -0.9692660, 0.0556434,
                                -1.5371385,  1.8760108, -0.2040259,
                                -0.4985314, 0.0415560, 1.0572252);

fragment half4 xyz12RenderFragment(SingleInputVertexIO fragmentInput [[stage_in]],
                                     texture2d<half> textureXYZ12 [[texture(0)]])
{
    //https://stackoverflow.com/questions/43494018/converting-xyz-color-to-rgb
    constexpr sampler quadSampler;
    
    half3 xyz_gamma = half3(2.6);
    half3 rgb_gamma = half3(1.0/2.2);
    half3 v_in = textureXYZ12.sample(quadSampler, fragmentInput.textureCoordinate).rgb;
    v_in = pow(v_in, xyz_gamma);
    half3 v_out = matrix_xyz_rgb * v_in ;
    v_out = pow(v_out, rgb_gamma) ;
    v_out = clamp(v_out, 0.0, 1.0) ;
    
    return half4(v_out, 1.0);
}




fragment half4 soft_I420RenderFragment(SingleInputVertexIO fragmentInput [[stage_in]],
                                     texture2d<half> textureY [[texture(0)]],
                                     texture2d<half> textureU [[texture(1)]],
                                     texture2d<half> textureV [[texture(2)]])
{
    constexpr sampler quadSampler;
    half3 yuv;
    yuv.x = textureY.sample(quadSampler, fragmentInput.textureCoordinate).r;
    yuv.y = textureU.sample(quadSampler, fragmentInput.textureCoordinate).r - 0.5;
    yuv.z = textureV.sample(quadSampler, fragmentInput.textureCoordinate).r - 0.5;
    
    return half4(colorConversionMatrix709Default * yuv, 1);
}
