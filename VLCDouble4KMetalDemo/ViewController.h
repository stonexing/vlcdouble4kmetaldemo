//
//  ViewController.h
//  VLCDouble4KMetalDemo
//
//  Created by a on 2021/7/20.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (readwrite, retain) IBOutlet UIView *videoViewOne;
@property (readwrite, retain) IBOutlet UIView *videoViewTwo;
@property (readwrite, retain) IBOutlet UIView *videoViewThree;
@property (readwrite, retain) IBOutlet UIView *videoViewFour;

@property (weak, nonatomic) IBOutlet UILabel *asptoLabel;
@property (weak, nonatomic) IBOutlet UISwitch *forceUseOpenGLSwitch;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *pauseButton;

@end

