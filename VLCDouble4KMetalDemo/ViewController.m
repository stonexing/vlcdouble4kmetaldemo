//
//  ViewController.m
//  VLCDouble4KMetalDemo
//
//  Created by a on 2021/7/20.
//

#import "ViewController.h"

#import "VLCMedia.h"
//

@interface ViewController ()
{
    VLCMedia *_mediaOne;
    VLCMediaPlayer *_mediaPlayerOne;
    VLCMedia *_mediaTwo;
    VLCMediaPlayer *_mediaPlayerTwo;
    VLCMedia *_mediaThree;
    VLCMediaPlayer *_mediaPlayerThree;
    VLCMedia *_mediaFour;
    VLCMediaPlayer *_mediaPlayerFour;
    
    NSUInteger _currentAspectRatio;
    BOOL _isInFillToScreen;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // FIXME: set this to your own sample file
    NSURL *mediaURL = [NSURL fileURLWithPath:@"/Users/asus/Downloads/\[极限运动\]LG\ 4K\ demo\ Skyliner\ 4K\ 3D\ x264\ \[www.4k123.com\].mkv"];//[[NSBundle mainBundle] URLForResource:@"IMG_1456" withExtension:@"MOV"];
    mediaURL = [NSURL fileURLWithPath:@"/Users/asus/Downloads/wondergirl.MOV"];
    
    mediaURL = [NSURL fileURLWithPath:@"/Users/a/Desktop/nian5.mov"];
//    mediaURL = [NSURL URLWithString:@"http://192.168.1.1:8200/MediaItems/12294.mov"];//h.265
    //mediaURL = [NSURL URLWithString:@"http://192.168.1.1:8200/MediaItems/12295.mov"];//h.264

    _mediaOne = [VLCMedia mediaWithURL:mediaURL];
    _mediaPlayerOne = [[VLCMediaPlayer alloc] init];
    _mediaPlayerOne.drawable = self.videoViewOne;
    _mediaPlayerOne.media = _mediaOne;

    _mediaTwo = [VLCMedia mediaWithURL:mediaURL];
    _mediaPlayerTwo = [[VLCMediaPlayer alloc] init];
    _mediaPlayerTwo.drawable = self.videoViewTwo;
    _mediaPlayerTwo.media = _mediaTwo;
    _mediaPlayerTwo.libraryInstance.debugLogging = YES;

    _mediaThree = [VLCMedia mediaWithURL:mediaURL];
    _mediaPlayerThree = [[VLCMediaPlayer alloc] init];
    _mediaPlayerThree.drawable = self.videoViewThree;
    _mediaPlayerThree.media = _mediaTwo;

    _mediaFour = [VLCMedia mediaWithURL:mediaURL];
    _mediaPlayerFour = [[VLCMediaPlayer alloc] init];
    _mediaPlayerFour.drawable = self.videoViewFour;
    _mediaPlayerFour.media = _mediaFour;

    UITapGestureRecognizer *tapGestureOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playOne:)];
    [self.videoViewOne addGestureRecognizer:tapGestureOne];
    UITapGestureRecognizer *tapGestureTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playTwo:)];
    [self.videoViewTwo addGestureRecognizer:tapGestureTwo];
    UITapGestureRecognizer *tapGestureThree = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playThree:)];
    [self.videoViewThree addGestureRecognizer:tapGestureThree];
    UITapGestureRecognizer *tapGestureFour = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playFour:)];
    [self.videoViewFour addGestureRecognizer:tapGestureFour];
    
    
    _forceUseOpenGLSwitch.on = [NSUserDefaults.standardUserDefaults integerForKey:@"force_use_epengl_apple"] == 1;
}

- (void)playOne:(UITapGestureRecognizer *)sender
{
    if (_mediaPlayerOne.isPlaying) {
        [_mediaPlayerOne stop];
    } else {
        [_mediaPlayerOne play];
    }
}

- (void)playTwo:(UITapGestureRecognizer *)sender
{
    if (_mediaPlayerTwo.isPlaying) {
        [_mediaPlayerTwo stop];
    } else {
        [_mediaPlayerTwo play];
    }
}

- (void)playThree:(UITapGestureRecognizer *)sender
{
    if (_mediaPlayerThree.isPlaying) {
        [_mediaPlayerThree stop];
    } else {
        [_mediaPlayerThree play];
    }
}

- (void)playFour:(UITapGestureRecognizer *)sender
{
    if (_mediaPlayerFour.isPlaying) {
        [_mediaPlayerFour stop];
    } else {
        [_mediaPlayerFour play];
    }
}

- (IBAction)changeOpenGLSetting:(UISwitch *)sender {
    if(sender.on){
        [NSUserDefaults.standardUserDefaults setInteger:1 forKey:@"force_use_epengl_apple"];
    }else{
        [NSUserDefaults.standardUserDefaults setInteger:0 forKey:@"force_use_epengl_apple"];
    }
    [NSUserDefaults.standardUserDefaults synchronize];
}

- (IBAction)buttonAspetChanged:(id)sender {
   
    
        
    _currentAspectRatio = _currentAspectRatio == VLCAspectRatioSixteenToTen ? VLCAspectRatioDefault : _currentAspectRatio + 1;
    NSString *asptoString = [self stringForAspectRatio:_currentAspectRatio];
    self.asptoLabel.text = asptoString;
    //return;
        // If fullScreen is toggled directly and then the aspect ratio changes, fullScreen is not reset
        if (_isInFillToScreen) _isInFillToScreen = NO;

        switch (_currentAspectRatio) {
            case VLCAspectRatioDefault:
                _mediaPlayerOne.scaleFactor = 0;
                _mediaPlayerOne.videoAspectRatio = NULL;
                break;
            case VLCAspectRatioFillToScreen:
                // Reset aspect ratio only with aspectRatio button since we want to keep
                // the user ratio with double tap.
                _mediaPlayerOne.videoAspectRatio = NULL;
                [self switchToFillToScreen];
                break;
            case VLCAspectRatioFourToThree:
            case VLCAspectRatioSixteenToTen:
            case VLCAspectRatioSixteenToNine:
                _mediaPlayerOne.scaleFactor = 0;
//                _mediaPlayerOne.videoCropGeometry = NULL;
//                _mediaPlayerOne.videoAspectRatio = (char *)[[self stringForAspectRatio:_currentAspectRatio] UTF8String];
               
                NSLog(@"视频比例设定%@", asptoString);
                _mediaPlayerOne.videoAspectRatio = (char *)[asptoString UTF8String];
        }


    
}
- (NSString *)stringForAspectRatio:(VLCAspectRatio)ratio
{
    switch (ratio) {
            case VLCAspectRatioFillToScreen:
            return NSLocalizedString(@"FILL_TO_SCREEN", nil);
            case VLCAspectRatioDefault:
            return NSLocalizedString(@"DEFAULT", nil);
            case VLCAspectRatioFourToThree:
            return @"4:3";
            case VLCAspectRatioSixteenToTen:
            return @"16:10";
            case VLCAspectRatioSixteenToNine:
            return @"16:9";
        default:
            NSAssert(NO, @"this shouldn't happen");
    }
}

- (UIScreen *)currentScreen
{
    return [UIScreen mainScreen];
}
- (void)switchToFillToScreen
{
    UIScreen *screen = [self currentScreen];
    CGSize screenSize = screen.bounds.size;

    CGSize videoSize = _mediaPlayerOne.videoSize;

    CGFloat ar = videoSize.width / (float)videoSize.height;
    CGFloat dar = screenSize.width / (float)screenSize.height;

    CGFloat scale;

    if (dar >= ar) {
        scale = screenSize.width / (float)videoSize.width;
    } else {
        scale = screenSize.height / (float)videoSize.height;
    }

    // Multiplied by screen.scale in consideration of pt to px
    _mediaPlayerOne.scaleFactor = scale * screen.scale;
    _isInFillToScreen = YES;
}


- (IBAction)brightnessChanged:(UISlider *)sender {
    switch (sender.tag) {
        case 0:
            _mediaPlayerOne.brightness = sender.value;
            break;
        case 1:
            _mediaPlayerOne.contrast = sender.value;
            break;
        case 2:
            _mediaPlayerOne.hue = sender.value;
            break;
        case 3:
            _mediaPlayerOne.saturation = sender.value;
            break;
        case 4:
            _mediaPlayerOne.gamma = sender.value;
            break;
            
        default:
            break;
    }
}

- (IBAction)playButtonPressed:(UIButton *)sender {
    if (_mediaPlayerOne.isPlaying) {
        [_mediaPlayerOne stop];
        [_playButton setHidden:NO];
        [_pauseButton setHidden:YES];
        
    } else {
        [_mediaPlayerOne play];
        [_playButton setHidden:YES];
        [_pauseButton setHidden:NO];
        
    }
}


@end
