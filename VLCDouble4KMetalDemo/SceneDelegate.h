//
//  SceneDelegate.h
//  VLCDouble4KMetalDemo
//
//  Created by a on 2021/7/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

